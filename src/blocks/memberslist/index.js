import { registerBlockType } from "@wordpress/blocks/src";
import { TextControl, TimePicker, DateTimePicker } from "@wordpress/components";
import { Component } from "@wordpress/element/src";

import List from "../../components/list";
import Cleave from "cleave.js/react";
import "cleave.js/dist/addons/cleave-phone.de";

class MembersListListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: ""
    };
  }

  render() {
    const props = this.props.value;

    if (this.props.isEditor) {
      return (
        <div className="list-item">
          <TextControl
            label="Mitglieds Name"
            value={props.name}
            placeholder={props.name}
            onChange={name => this.props.onUpdateItem("name", name)}
          />
          <label
            className="components-base-control"
            htmlFor={`member-phonenumber-${props.id}`}
            style={{
              display: "block",
              marginBottom: 0
            }}
          >
            Mitglieds Telefonnummer
          </label>
          <Cleave
            id={`member-phonenumber-${props.id}`}
            options={{ phone: true, phoneRegionCode: "DE" }}
            value={props.phonenumber}
            onChange={ev =>
              this.props.onUpdateItem("phonenumber", ev.target.value)
            }
          />
          <label
            className="components-base-control"
            htmlFor={`member-birthday-${props.id}`}
            style={{
              display: "block",
              marginBottom: -10,
              marginTop: 5
            }}
          >
            Mitglieds Geburtstag
          </label>
          <TimePicker
            label="Mitglieds Geburtstag"
            id={`member-birthday-${props.id}`}
            value={props.birthday}
            placeholder={props.birthday}
            onChange={birthday => this.props.onUpdateItem("birthday", birthday)}
          />
        </div>
      );
    } else {
      return (
        <tr className={this.props.className}>
          <td>{props.name}</td>
          <td>{new Date(props.birthday).toLocaleDateString()}</td>
          <td>
            <a href={`tel:${props.phonenumber}`}>{props.phonenumber}</a>
          </td>
        </tr>
      );
    }
  }
}

registerBlockType("lionsclub/memberslist", {
  title: "LC - Mitgliederliste",
  icon: "group",
  category: "layout",
  attributes: {
    list: {
      type: "array"
    }
  },
  edit(props) {
    const {
      attributes: { list },
      setAttributes
    } = props;

    return (
      <div>
        <h2>Mitgliederliste</h2>
        <List
          list={list}
          id="mitglieder"
          listItem={MembersListListItem}
          className="lionsclub-block"
          onChange={(type, data) => setAttributes({ [type]: data })}
          isEditor
        />
      </div>
    );
  },
  save({ attributes: { list }, className }) {
    return (
      <section id="mitglieder" className="content">
        <List
          list={list}
          tagName="table"
          listItem={MembersListListItem}
          className={className}
        />
      </section>
    );
  }
});
