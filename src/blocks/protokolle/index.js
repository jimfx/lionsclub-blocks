import { registerBlockType } from "@wordpress/blocks/build-module";
import { TextControl } from "@wordpress/components";
import { RichText } from "@wordpress/block-editor/build-module";
import { Component } from "@wordpress/element/build-module";

import List from "../../components/list";
import PDF from "../../components/pdf";

class ProtocolListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: ""
    };
  }

  render() {
    const props = this.props.value;

    if (this.props.isEditor) {
      return (
        <div className="list-item">
          <h3>
            <TextControl
              tagName="h3"
              label="Protokoll Titel"
              value={props.titel}
              placeholder={props.titel}
              onChange={titel => this.props.onUpdateItem("titel", titel)}
            />
          </h3>
          <p>Protokoll Inhalt</p>
          <RichText
            tagName="p"
            formattingControls={[]}
            multiline={true}
            placeholder="Protokoll Inhalt"
            title="Titel"
            value={props.content}
            label="Überschrift"
            onChange={content => this.props.onUpdateItem("content", content)}
          />
          <p>Protokoll PDF</p>
          <PDF
            file={props.pdf}
            onChange={pdf => this.props.onUpdateItem("pdf", pdf)}
            isEditor={this.props.isEditor}
          />
        </div>
      );
    } else {
      return (
        <div className="protokoll-wrapper">
          <div className="protokoll-header">
            {props.content && (
              <img src="/wp-content/themes/lionsclub-theme/assets/icons/left_arrow.svg" />
            )}
            <h3>{props.titel}</h3>
            {props.pdf && (
              <a href={props.pdf.url} target="_blank" rel="noopener noreferrer">
                PDF herunterladen
              </a>
            )}
          </div>

          {props.content && (
            <div className="protokoll-content">
              <RichText.Content value={props.content} />
            </div>
          )}
        </div>
      );
    }
  }
}

registerBlockType("lionsclub/protocols", {
  title: "LC - Protokolle",
  icon: "format-aside",
  category: "layout",
  attributes: {
    buttonText: {
      type: "string"
    },
    list: {
      type: "array"
    }
  },
  edit(props) {
    const {
      attributes: { list },
      setAttributes
    } = props;

    return (
      <div>
        <h2>Protokolle</h2>
        <List
          list={list}
          listItem={ProtocolListItem}
          className="lionsclub-block"
          onChange={(type, data) => setAttributes({ [type]: data })}
          isEditor
        />
      </div>
    );
  },
  save({ attributes: { list, buttonText }, className }) {
    return (
      <section id="protokolle" className="content">
        <List
          list={list}
          props={{ buttonText: buttonText }}
          listItem={ProtocolListItem}
          className={className}
        />
      </section>
    );
  }
});
