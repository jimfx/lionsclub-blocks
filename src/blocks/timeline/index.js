import { registerBlockType } from "@wordpress/blocks/build-module";
import { RichText } from "@wordpress/block-editor/build-module";
import { TextControl } from "@wordpress/components/build-module";
import { Component } from "@wordpress/element/build-module";

import List from "../../components/list";
import Image from "../../components/image";

class TimelineListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: ""
    };
  }

  render() {
    const props = this.props.value;

    return (
      <div className={"list-item"}>
        {/* EL: TITLE */}
        {this.props.isEditor ? (
          <h4>
            <TextControl
              tagName="h4"
              label="Überschrift"
              value={props.title}
              placeholder={props.title}
              onChange={title => this.props.onUpdateItem("title", title)}
            />
          </h4>
        ) : (
          <h4 className="heading">{props.title}</h4>
        )}

        {/* EL: IMAGE */}
        <Image
          image={props.image}
          onChange={newImage => {
            this.props.onUpdateItem("image", newImage);
            console.log(newImage);
          }}
          isEditor={this.props.isEditor}
        />

        {/* EL: SUBLINE */}
        {this.props.isEditor ? (
          <TextControl
            label="Untertitel (Optional)"
            value={props.subline}
            placeholder={props.subline}
            onChange={subline => this.props.onUpdateItem("subline", subline)}
          />
        ) : (
          <span className="subline">{props.subline}</span>
        )}

        {/* EL: YEAR */}
        {this.props.isEditor ? (
          <TextControl
            label="Jahr"
            type="number"
            min="1800"
            max="2100"
            value={props.year}
            placeholder={props.year}
            onChange={year => this.props.onUpdateItem("year", year)}
          />
        ) : (
          <div className="year">{props.year}</div>
        )}
      </div>
    );
  }
}

registerBlockType("lionsclub/timeline", {
  title: "LC - Timeline",
  icon: "clock",
  category: "layout",
  attributes: {
    list: {
      type: "array"
    },
    title: {
      type: "string"
    }
  },
  edit(props) {
    const {
      attributes: { list, title },
      setAttributes
    } = props;

    return (
      <div>
        <RichText
          tagName="h2"
          formattingControls={[]}
          multiline={false}
          style={{
            textAlign: "center"
          }}
          title="Titel"
          value={title || "Überschrift"}
          label="Überschrift"
          onChange={value => setAttributes({ title: value })}
        />
        <List
          list={list}
          listItem={TimelineListItem}
          className="lionsclub-block"
          onChange={(type, data) => setAttributes({ [type]: data })}
          isEditor
        />
      </div>
    );
  },
  save({ attributes: { list, title }, className }) {
    return (
      <div className={className}>
        <h2>{title}</h2>
        <List list={list} listItem={TimelineListItem} className={className} />
      </div>
    );
  }
});
