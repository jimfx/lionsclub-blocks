window.addEventListener("load", function() {
  Array.prototype.forEach ||
    (Array.prototype.forEach = function(r, o) {
      var t, n;
      if (null === this) throw new TypeError(" this is null or not defined");
      var e = Object(this),
        i = e.length >>> 0;
      if ("function" != typeof r) throw new TypeError(r + " is not a function");
      for (1 < arguments.length && (t = o), n = 0; n < i; ) {
        var f;
        n in e && ((f = e[n]), r.call(t, f, n, e)), n++;
      }
    });

  function show(el) {
    el.classList.remove("list-item-hidden");
  }

  var supportsIntersectionObserver = "IntersectionObserver" in window;

  var elements = [];

  Array.prototype.forEach.call(
    document.getElementsByClassName("list-item"),
    function(slide) {
      elements.push(slide);
      if (supportsIntersectionObserver) {
        slide.classList.add("list-item-hidden");
      }
    }
  );

  if (supportsIntersectionObserver) {
    const handleObserve = entries =>
      entries.forEach(entry => {
        if (entry.intersectionRatio > 0) {
          show(entry.target);
          observer.unobserve(entry.target);
        }
      });

    const observer = new IntersectionObserver(handleObserve, {
      threshold: 0.75
    });

    elements.forEach(elem => {
      observer.observe(elem);
    });
  } else {
  }
});
