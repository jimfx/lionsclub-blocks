import { registerBlockType } from "@wordpress/blocks/build-module";
import { TextControl } from "@wordpress/components";
import { RichText } from "@wordpress/block-editor/build-module";
import { Component } from "@wordpress/element/build-module";

import List from "../../components/list";
import PDF from "../../components/pdf";

class FormListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: ""
    };
  }

  render() {
    const props = this.props.value;

    if (this.props.isEditor) {
      return (
        <div className="list-item">
          <PDF
            file={props.pdf}
            onChange={pdf => this.props.onUpdateItem("pdf", pdf)}
            isEditor={this.props.isEditor}
          />
          <TextControl
            tagName="p"
            label="Formular Beschreibung"
            value={props.description}
            placeholder={props.description}
            className="formulare-description"
            onChange={description =>
              this.props.onUpdateItem("description", description)
            }
          />
        </div>
      );
    } else {
      return (
        <a
          href={props.pdf.url}
          target="_blank"
          rel="noopener noreferrer"
          style={{
            backgroundImage: `url(${props.pdf.sizes.large.url ||
              props.pdf.sizes.medium_large.url})`
          }}
        >
          <p>{props.description}</p>
        </a>
      );
    }
  }
}

registerBlockType("lionsclub/forms", {
  title: "LC - Formulare",
  icon: "feedback",
  category: "layout",
  attributes: {
    buttonText: {
      type: "string"
    },
    list: {
      type: "array"
    }
  },
  edit(props) {
    const {
      attributes: { list },
      setAttributes
    } = props;

    return (
      <div>
        <h2>Formulare</h2>
        <List
          list={list}
          listItem={FormListItem}
          className="lionsclub-block"
          onChange={(type, data) => setAttributes({ [type]: data })}
          isEditor
        />
      </div>
    );
  },
  save({ attributes: { list, buttonText }, className }) {
    return (
      <section id="formulare" className="content">
        <List
          list={list}
          props={{ buttonText: buttonText }}
          listItem={FormListItem}
          className={className}
        />
      </section>
    );
  }
});
