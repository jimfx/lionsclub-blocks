import { registerBlockType } from "@wordpress/blocks/src";
import { TextControl, PanelBody, PanelRow } from "@wordpress/components";
import { InspectorControls } from "@wordpress/block-editor/src";
import { Component } from "@wordpress/element/src";

import List from "../../components/list";
import Image from "../../components/image";

class MembersListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: ""
    };
  }

  render() {
    const props = this.props.value;

    const hasImage = "image" in props && !!props.image;
    const imageUrl = hasImage ? props.image.sizes.medium.url : "";

    if (this.props.isEditor) {
      return (
        <div className="list-item">
          <TextControl
            label="Projekt Name"
            value={props.name}
            placeholder={props.name}
            onChange={name => this.props.onUpdateItem("name", name)}
          />
          <TextControl
            label="Projekt URL"
            value={props.url}
            placeholder={props.url}
            onChange={url => this.props.onUpdateItem("url", url)}
          />
          <Image
            image={props.image}
            onChange={image => this.props.onUpdateItem("image", image)}
            isEditor={this.props.isEditor}
          />
        </div>
      );
    } else {
      return (
        <section
          className={this.props.className}
          style={{
            backgroundImage: `url(${imageUrl})`
          }}
        >
          <div className="right">
            <h2>{props.name}</h2>
            <a className="primary" href={props.url}>
              <h3>{props.buttonText}</h3>
            </a>
          </div>
        </section>
      );
    }
  }
}

registerBlockType("lionsclub/projects", {
  title: "LC - Projekte",
  icon: "smiley",
  category: "layout",
  attributes: {
    buttonText: {
      type: "string"
    },
    list: {
      type: "array"
    }
  },
  edit(props) {
    const {
      attributes: { list, buttonText },
      setAttributes
    } = props;

    return (
      <div>
        <InspectorControls>
          <PanelBody title="Einstellungen">
            <PanelRow>
              <TextControl
                label="Button Text"
                value={buttonText}
                placeholder="Spenden"
                onChange={ev => setAttributes({ buttonText: ev })}
              />
            </PanelRow>
          </PanelBody>
        </InspectorControls>
        <List
          list={list}
          listItem={MembersListItem}
          props={{ buttonText: buttonText }}
          className="lionsclub-block"
          onChange={(type, data) => setAttributes({ [type]: data })}
          isEditor
        />
      </div>
    );
  },
  save({ attributes: { list, buttonText }, className }) {
    return (
      <List
        list={list}
        props={{ buttonText: buttonText }}
        listItem={MembersListItem}
        className={className}
      />
    );
  }
});
