import { registerBlockType } from "@wordpress/blocks/src";
import { TextControl } from "@wordpress/components";
import { RichText } from "@wordpress/block-editor/src";
import { Component } from "@wordpress/element/src";

import List from "../../components/list";
import Image from "../../components/image";

class MembersListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: ""
    };
  }

  render() {
    const props = this.props.value;

    const hasImage = "image" in props && !!props.image;
    const imageUrl = hasImage ? props.image.sizes.medium.url : "";

    if (this.props.isEditor) {
      return (
        <div className="list-item">
          <TextControl
            label="Name"
            value={props.name}
            placeholder={props.name}
            onChange={name => this.props.onUpdateItem("name", name)}
          />
          <TextControl
            label="Titel"
            value={props.titel}
            placeholder={props.titel}
            onChange={titel => this.props.onUpdateItem("titel", titel)}
          />
          <RichText
            tagName="p"
            formattingControls={[]}
            multiline={true}
            style={{
              textAlign: "center"
            }}
            placeholder="Zitat"
            title="Titel"
            value={props.quote}
            label="Überschrift"
            onChange={quote => this.props.onUpdateItem("quote", quote)}
          />
          <Image
            image={props.image}
            onChange={image => this.props.onUpdateItem("image", image)}
            isEditor={this.props.isEditor}
          />
        </div>
      );
    } else {
      return (
        <section className={this.props.className}>
          <div
            className="left"
            style={{
              backgroundImage: `url(${imageUrl})`
            }}
          >
            <p>
              <b>{props.name}</b>
              {props.titel}
            </p>
          </div>
          <RichText.Content tagName="p" value={props.quote} />
        </section>
      );
    }
  }
}

registerBlockType("lionsclub/members", {
  title: "LC - Mitglieder",
  icon: "groups",
  category: "layout",
  attributes: {
    list: {
      type: "array"
    }
  },
  edit(props) {
    const {
      attributes: { list },
      setAttributes
    } = props;
    return (
      <List
        list={list}
        listItem={MembersListItem}
        className="lionsclub-block"
        onChange={(type, data) => setAttributes({ [type]: data })}
        isEditor
      />
    );
  },
  save({ attributes: { list }, className }) {
    return (
      <List list={list} listItem={MembersListItem} className={className} />
    );
  }
});
