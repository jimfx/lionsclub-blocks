import { Tooltip, Button, Dashicon } from "@wordpress/components";
import { Component } from "@wordpress/element/src";

import id from "shortid";

/**
 * List Component
 *
 * @author [Max Richter](https://github.com/jim-fx)
 *  */

export default class List extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: props.title || "Titel",
      list: props.list || []
    };
  }

  updateList(newList) {
    this.setState({ list: newList });
    this.props.onChange("list", newList);
  }

  removeItem(i) {
    const newList = this.state.list.splice(0);
    newList.splice(i, 1);
    this.updateList(newList);
  }

  addItem(i = this.state.list.length) {
    const newList = this.state.list.splice(0);
    newList.splice(i, 0, {
      id: id(),
      title: undefined,
      year: 1990,
      subline: undefined,
      image: undefined
    });
    this.updateList(newList);
  }

  swapItems(oldI, newI) {
    const tempA = this.state.list[newI];
    const newList = this.state.list.slice(0);
    newList[newI] = newList[oldI];
    newList[oldI] = tempA;
    this.updateList(newList);
  }

  updateItem(i, type, data) {
    const newList = this.state.list.splice(0);
    newList[i][type] = data;
    this.updateList(newList);
  }

  updateTitle(value) {
    this.setState({ title: value });
    "onChange" in this.props && this.props.onChange("title", value);
  }

  render() {
    const CustomTag = this.props.tagName || "div";

    return (
      <CustomTag
        id={this.props.id}
        className={`lionsclub-list ${this.props.className}`}
      >
        {this.props.isEditor
          ? this.state.list.map((v, i) => {
              return (
                <div className="list-item" style={{ position: "relative" }}>
                  {/* EL: ADD NEW ITEM */}
                  <Tooltip text="Neues Element hinzufügen">
                    <Button
                      className="add-item"
                      onClick={() => this.addItem(i)}
                    >
                      <Dashicon icon="insert" />
                    </Button>
                  </Tooltip>

                  {/* EL: MOVE UP ICON */}
                  {i > 0 && (
                    <Tooltip text="Element nach oben bewegen">
                      <Button
                        className="move-item-up"
                        onClick={() => this.swapItems(i, i - 1)}
                      >
                        <Dashicon icon="arrow-up-alt2" />
                      </Button>
                    </Tooltip>
                  )}

                  {/* EL: REMOVE ICON */}
                  <Tooltip text="Element entfernen">
                    <Button
                      className="remove-item"
                      onClick={() => this.removeItem(i)}
                    >
                      <Dashicon icon="trash" />
                    </Button>
                  </Tooltip>

                  {/* EL: MOVE DOWN ICON */}
                  {i < this.state.list.length - 1 && (
                    <Tooltip text="Element nach unten bewegen">
                      <Button
                        className="move-item-down"
                        onClick={() => this.swapItems(i, i + 1)}
                      >
                        <Dashicon icon="arrow-down-alt2" />
                      </Button>
                    </Tooltip>
                  )}

                  {/* EL: ACTUAL LIST ITEM */}
                  <div className="list-item-wrapper">
                    <this.props.listItem
                      key={v.id}
                      isEditor={this.props.isEditor}
                      onUpdateItem={(type, data) =>
                        this.updateItem(i, type, data)
                      }
                      value={Object.assign(v, this.props.props)}
                    />
                  </div>
                </div>
              );
            })
          : this.state.list.map((v, i) => {
              return <this.props.listItem key={v.id} value={v} />;
            })}
        {this.props.isEditor && (
          <Tooltip text="Neues Element hinzufügen">
            <Button
              className="add-item"
              onClick={() => {
                this.addItem();
              }}
            >
              <Dashicon icon="insert" />
            </Button>
          </Tooltip>
        )}
      </CustomTag>
    );
  }
}
