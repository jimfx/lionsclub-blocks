import { MediaPlaceholder } from "@wordpress/block-editor/src";
import { Dashicon } from "@wordpress/components";
import { Component } from "@wordpress/element/src";

export default class Image extends Component {
  constructor(props) {
    super(props);
  }

  reset() {
    this.props.onChange(undefined);
  }

  render() {
    const image = this.props.image;
    const hasImage = !!image;
    const isEditor = !!this.props.isEditor;
    const imageURL = hasImage
      ? image.sizes.medium.url || image.sizes.medium_large.url
      : "https://via.placeholder.com/300x200";

    return isEditor && !hasImage ? (
      <MediaPlaceholder
        onSelect={el => {
          this.props.onChange(el);
        }}
        allowedTypes={["image"]}
        multiple={false}
        labels={{ title: "Bild", instructions: " " }}
      />
    ) : isEditor ? (
      <div
        className="image-icon"
        style={{
          backgroundImage: `url(${imageURL})`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          backgroundPosition: "center"
        }}
      >
        <div className="image-icon-trash">
          <button onClick={ev => this.reset()}>
            <Dashicon icon="no" />
          </button>
        </div>
      </div>
    ) : (
      <img src={imageURL} />
    );
  }
}
