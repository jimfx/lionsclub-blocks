import { MediaPlaceholder } from "@wordpress/block-editor/src";
import { Dashicon } from "@wordpress/components";
import { Component } from "@wordpress/element/src";

export default class Image extends Component {
  constructor(props) {
    super(props);
  }

  reset() {
    this.props.onChange(undefined);
  }

  render() {
    const pdf = this.props.file;
    const hasPDF = !!pdf && !!pdf.sizes;
    const isEditor = !!this.props.isEditor;
    const pdfThumbnailUrl = hasPDF
      ? pdf.sizes.large.url || pdf.sizes.medium_large.url
      : "https://via.placeholder.com/100x200";

    return isEditor && !hasPDF ? (
      <MediaPlaceholder
        onSelect={el => {
          if ("id" in el) {
            this.props.onChange(el);
          }
        }}
        allowedTypes={["application/pdf"]}
        multiple={false}
        labels={{ title: "PDF", instructions: "" }}
      />
    ) : isEditor ? (
      <div
        className="image-icon"
        style={{
          backgroundImage: `url(${pdfThumbnailUrl})`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "contain"
        }}
      >
        <div className="image-icon-trash">
          <button onClick={ev => this.reset()}>
            <Dashicon icon="no" />
          </button>
        </div>
      </div>
    ) : (
      <img src={pdfThumbnailUrl} />
    );
  }
}
