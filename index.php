<?php

/**
 * Plugin Name: Custom Lionsclub Blocks
 * Description: This are custom blocks for the lionsclub wordpress.
 * Version: 1.0.0
 * Author: Max Richter
 *
 * @package  lionsclub
 */


/**
 * Registers all block assets so that they can be enqueued through Gutenberg in
 * the corresponding context.
 *
 * Passes translations to JavaScript.
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

function lionsclub_register_block() {

	wp_register_script(
		'lionsclub-timeline',
		plugins_url( 'build/index.js', __FILE__ ),
		array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-components', 'wp-editor' ),
		filemtime( plugin_dir_path( __FILE__ ) . 'build/index.js' )
	);

	register_block_type( 'lionsclub/timeline', array(
		'editor_script' => 'lionsclub-timeline',
	) );

}
add_action( 'init', 'lionsclub_register_block' );

function lionsclub_register_block_assets(){

	// REGISTER TIMELINE STYLE AND SCRIPT
	wp_enqueue_style(
		'lionsclub/timeline-styles',
        plugins_url( 'src/index.css', __FILE__ ),
        array( 'wp-edit-blocks' )
	);
	wp_enqueue_script(
		'lionsclub/timeline-script',
		plugins_url( '/src/blocks/timeline/block-timeline.js', __FILE__ ),
		filemtime( plugin_dir_path( __FILE__ ) . '/src/blocks/timeline/block-timeline.js' ) 
);
}
add_action( 'enqueue_block_assets', 'lionsclub_register_block_assets' );

function liosnclub_register_editor_styles() {
	wp_enqueue_style(
	'lionsclub/timeline-editor',
			plugins_url( 'src/editor.css', __FILE__ ),
			array( 'wp-edit-blocks' )
	);
};

add_action( 'enqueue_block_editor_assets', 'liosnclub_register_editor_styles');